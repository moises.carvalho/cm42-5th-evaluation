import { Entry } from "../domain/Entry";
import { EntryType } from "../domain/EntryType";
import { ApiEntry } from "./ApiEntry";
import { CreateEntryDto } from "./CreateEntryDto";

export const createEntry = async (
  createEntryDto: CreateEntryDto
): Promise<Entry> => {
  const response = await fetch("http://localhost:3000/api/entries", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(mapCreateEntryDtoToApiEntry(createEntryDto)),
  });
  const data = await response.json();

  return { ...data, key: Math.random().toString() };
};

const mapCreateEntryDtoToApiEntry = (entry: CreateEntryDto): ApiEntry => ({
  amount: entry.amount,
  currency: entry.currency,
  date: Date.now(),
  ledger: entry.ledger,
  type: entry.type === EntryType.In ? "in" : "out",
});
