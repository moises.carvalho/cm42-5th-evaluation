import { Entry } from "../domain/Entry";
import { EntryType } from "../domain/EntryType";
import { ApiEntry } from "./ApiEntry";

export type FetchEntriesParameters = {
  page?: number;
  inCurrency?: string;
};

export const fetchEntries = async ({
  inCurrency,
  page,
}: FetchEntriesParameters): Promise<Array<Entry>> => {
  const response = await fetch(
    `http://localhost:3000/api/entries?page=${page}${
      inCurrency ? `&in_currency=${inCurrency}` : ""
    }`
  );
  const data = await response.json();

  return mapApiEntriesToEntries(data);
};

const mapApiEntriesToEntries = (apiEntries: Array<ApiEntry>): Array<Entry> => {
  return apiEntries.map((apiEntry) => ({
    key: Math.random().toString(),
    type: apiEntry.type === "in" ? EntryType.In : EntryType.Out,
    ledger: apiEntry.ledger,
    amount: apiEntry.amount,
    currency: apiEntry.currency,
    date: new Date(apiEntry.date),
  }));
};
