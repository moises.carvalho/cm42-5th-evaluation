export const ApiEntryAllTypes = ["in", "out"] as const;

export type ApiEntryType = typeof ApiEntryAllTypes[number];

export type ApiEntry = {
  type: ApiEntryType;
  ledger: string;
  amount: number;
  currency: string;
  date: number;
};
