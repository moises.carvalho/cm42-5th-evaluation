import { Entry } from "../domain/Entry";

export type CreateEntryDto = Omit<Entry, "date" | "key">;
