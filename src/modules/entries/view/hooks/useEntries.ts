import { useCallback, useContext, useEffect, useRef } from "react";
import { Entry } from "../../domain/Entry";
import { CreateEntryDto } from "../../infrastructure/CreateEntryDto";
import { fetchEntries } from "../../infrastructure/fetchEntries";
import { createEntry as infraCreateEntry } from "../../infrastructure/createEntry";
import { EntriesContext } from "../components/EntriesProvider";

type UseEntriesParameters = {
  onFetchEntriesSuccess?: (entries: Array<Entry>) => void;
  onFetchEntriesError?: (error: unknown) => void;
  onCreateEntrySuccess?: (entries: Entry) => void;
  onCreateEntryError?: (error: unknown) => void;
};

export const useEntries = ({
  onFetchEntriesSuccess,
  onFetchEntriesError,
  onCreateEntrySuccess,
  onCreateEntryError,
}: UseEntriesParameters = {}) => {
  const value = useContext(EntriesContext);

  if (!value) {
    throw new Error(
      "Cannot call useEntries without having an EntriesProvider higher up in the tree!"
    );
  }

  const {
    entries,
    setEntries,
    page,
    setPage,
    inCurrency,
    setInCurrency,
    availableCurrencies,
    isFetching,
    setIsFetching,
    isCreating,
    setIsCreating,
    fetchEntriesError,
    setFetchEntriesError,
    createEntryError,
    setCreateEntryError,
  } = value;

  const onFetchEntriesSuccessRef = useRef(onFetchEntriesSuccess);
  onFetchEntriesSuccessRef.current = onFetchEntriesSuccess;

  const onFetchEntriesErrorRef = useRef(onFetchEntriesError);
  onFetchEntriesErrorRef.current = onFetchEntriesError;

  useEffect(() => {
    setIsFetching(true);

    fetchEntries({
      page,
      inCurrency,
    })
      .then((entries) => {
        setEntries(entries);
        setFetchEntriesError(undefined);
        onFetchEntriesSuccessRef.current?.(entries);
      })
      .catch((error) => {
        setFetchEntriesError(error);
        onFetchEntriesErrorRef.current?.(error);
      })
      .finally(() => {
        setIsFetching(false);
      });
  }, [page, inCurrency, setIsFetching, setEntries, setFetchEntriesError]);

  const createEntry = useCallback(
    async (data: CreateEntryDto): Promise<Entry> => {
      setIsCreating(true);

      try {
        const createdEntry = await infraCreateEntry(data);
        setCreateEntryError(undefined);
        onCreateEntrySuccess?.(createdEntry);

        return createdEntry;
      } catch (error) {
        setCreateEntryError(error);
        onCreateEntryError?.(error);

        throw error;
      } finally {
        setIsCreating(false);
      }
    },
    [
      onCreateEntryError,
      onCreateEntrySuccess,
      setCreateEntryError,
      setIsCreating,
    ]
  );

  return {
    entries,
    page,
    goToPage: setPage,
    inCurrency,
    availableCurrencies,
    convertToCurrency: setInCurrency,
    isFetching,
    createEntry,
    isCreating,
    fetchEntriesError,
    createEntryError,
  };
};
