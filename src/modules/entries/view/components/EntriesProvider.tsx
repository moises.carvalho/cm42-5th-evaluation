import { uniq } from "lodash";
import {
  createContext,
  Dispatch,
  ReactNode,
  SetStateAction,
  useMemo,
  useState,
} from "react";
import { Entry } from "../../domain/Entry";

type EntriesContextValue = {
  entries: Array<Entry> | undefined;
  setEntries: ReturnType<typeof useState<Array<Entry> | undefined>>[1];
  page: number;
  setPage: Dispatch<SetStateAction<number>>;
  inCurrency: string | undefined;
  setInCurrency: Dispatch<SetStateAction<string | undefined>>;
  availableCurrencies: Array<string>;
  isFetching: boolean;
  setIsFetching: Dispatch<SetStateAction<boolean>>;
  isCreating: boolean;
  setIsCreating: Dispatch<SetStateAction<boolean>>;
  fetchEntriesError: unknown;
  setFetchEntriesError: ReturnType<typeof useState<unknown>>[1];
  createEntryError: unknown;
  setCreateEntryError: ReturnType<typeof useState<unknown>>[1];
};

export const EntriesContext = createContext<EntriesContextValue | undefined>(
  undefined
);

type EntriesContextProviderProps = {
  children: ReactNode;
};

export const EntriesProvider = ({ children }: EntriesContextProviderProps) => {
  const [entries, setEntries] = useState<Array<Entry>>();
  const [page, setPage] = useState(1);
  const [inCurrency, setInCurrency] = useState<string | undefined>();
  const [isFetching, setIsFetching] = useState(false);
  const [isCreating, setIsCreating] = useState(false);
  const [fetchEntriesError, setFetchEntriesError] = useState<unknown>();
  const [createEntryError, setCreateEntryError] = useState<unknown>();

  const availableCurrencies = useMemo(() => ["USD", "EUR", "BRL"], []);

  const value = useMemo(
    () => ({
      entries,
      setEntries,
      page,
      setPage,
      inCurrency,
      setInCurrency,
      availableCurrencies,
      isFetching,
      setIsFetching,
      isCreating,
      setIsCreating,
      fetchEntriesError,
      setFetchEntriesError,
      createEntryError,
      setCreateEntryError,
    }),
    [
      createEntryError,
      entries,
      fetchEntriesError,
      inCurrency,
      availableCurrencies,
      isCreating,
      isFetching,
      page,
    ]
  );

  return (
    <EntriesContext.Provider value={value}>{children}</EntriesContext.Provider>
  );
};
