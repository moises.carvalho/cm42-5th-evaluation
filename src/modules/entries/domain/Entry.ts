import { EntryType } from "./EntryType";

export type Entry = {
  key: string;
  type: EntryType;
  ledger: string;
  amount: number;
  currency: string;
  date: Date;
};
