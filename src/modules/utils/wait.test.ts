import { wait } from "./wait";

describe("wait utils", () => {
  beforeAll(() => {
    jest.useFakeTimers();
    jest.spyOn(global, "setTimeout");
  });

  it("returns a promise that awaits", async () => {
    const promise = wait(5000);

    jest.runAllTimers();
    await promise;

    expect(setTimeout).toHaveBeenCalledTimes(1);
    expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 5000);
  });
});
