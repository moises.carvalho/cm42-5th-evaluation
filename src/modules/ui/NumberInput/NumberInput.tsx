import { ChangeEventHandler, useId } from "react";

import styles from "./NumberInput.module.scss";

export type NumberInputProps = {
  label?: string;
  name?: string;
  value?: number;
  onChange?: ChangeEventHandler<HTMLInputElement>;
};

export const NumberInput = ({
  label,
  name,
  onChange,
  value,
}: NumberInputProps) => {
  const id = useId();

  return (
    <div className={styles.container}>
      <label htmlFor={id} className={styles.label}>
        {label}
      </label>

      <input
        name={name}
        id={id}
        type="number"
        className={styles.input}
        value={value}
        onChange={onChange}
      />
    </div>
  );
};
