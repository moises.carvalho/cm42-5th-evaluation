import styles from "./Spinner.module.scss";

export type SpinnerProps = {};

export const Spinner = () => {
  return <div className={styles.spinner}>Wait for it...</div>;
};
