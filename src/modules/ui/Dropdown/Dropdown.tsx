import styles from "./Dropdown.module.scss";
import { useId } from "react";

export type DropdownProps<TOptions extends readonly any[]> = {
  value?: TOptions[number];
  onChange?: (value: TOptions[number]) => void;
  options: TOptions;
  name?: string;
  label?: string;
};

export function Dropdown<T extends readonly any[]>({
  options,
  name,
  onChange,
  value,
  label,
}: DropdownProps<T>) {
  const id = useId();

  return (
    <div className={styles.container}>
      <label htmlFor={id} className={styles.label}>
        {label}
      </label>

      <select
        name={name}
        className={styles.select}
        onChange={(e) => onChange?.(e.target.value)}
        value={value}
      >
        {options.map((option) => (
          <option key={option} value={option}>
            {option}
          </option>
        ))}
      </select>
    </div>
  );
}
