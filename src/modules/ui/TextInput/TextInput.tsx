import { ChangeEventHandler, useId } from "react";
import styles from "./TextInput.module.scss";

export type TextInputProps = {
  label?: string;
  name?: string;
  value?: string;
  onChange?: ChangeEventHandler<HTMLInputElement>;
};

export const TextInput = ({ label, name, onChange, value }: TextInputProps) => {
  const id = useId();

  return (
    <div className={styles.container}>
      <label htmlFor={id} className={styles.label}>
        {label}
      </label>

      <input
        name={name}
        id={id}
        className={styles.input}
        value={value}
        onChange={onChange}
      />
    </div>
  );
};
