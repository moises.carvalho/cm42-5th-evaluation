import styles from "./Header.module.scss";
import HomeIcon from "@/public/icons/home.svg";
import AddIcon from "@/public/icons/add.svg";
import DashboardIcon from "@/public/icons/dashboard.svg";
import Link from "next/link";
import { useRouter } from "next/router";
import cx from "classnames";

export const Header = () => {
  return (
    <>
      <header className={styles.header}>
        <nav>
          <ul className={styles.navLinkList}>
            <NavLink Icon={HomeIcon} link="/" text="Home" />

            <NavLink Icon={AddIcon} link="/add" text="Add" />

            <NavLink Icon={DashboardIcon} link="/dashboard" text="Dashboard" />
          </ul>
        </nav>
      </header>
    </>
  );
};

type NavLinkProps = {
  Icon: (props: { className: string }) => JSX.Element;
  link: string;
  text: string;
};

const NavLink = ({ Icon, link, text }: NavLinkProps) => {
  const router = useRouter();
  const activePath = router.asPath;
  const isActive = activePath === link;

  return (
    <li className={styles.navLink}>
      <Link className={styles.navLinkContent} href={link}>
        <Icon
          className={cx(styles.navLinkIcon, {
            [styles.active]: isActive,
          })}
        />

        <span
          className={cx(styles.navLinkText, {
            [styles.active]: isActive,
          })}
        >
          {text}
        </span>
      </Link>
    </li>
  );
};
