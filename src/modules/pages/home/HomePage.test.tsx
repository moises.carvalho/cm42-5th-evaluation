import { render, screen } from "@testing/components";

import type { Entry } from "@/modules/entries";
import { EntryType } from "@/modules/entries/domain/EntryType";
import { HomePage } from "@/modules/pages/home/HomePage";
import React from "react";
import { useEntries } from "@/modules/entries/view/hooks/useEntries";

jest.mock("@/modules/entries/view/hooks/useEntries");
jest.mock("@/modules/pages/home/EntryListFilter");
jest.mock("@/modules/pages/home/EntryList", () => {
  const EntryList = ({ entries }: { entries: Entry[] }) => {
    return (
      <ul>
        {entries.map((entry) => (
          <li key={entry.key}>{entry.ledger}</li>
        ))}
      </ul>
    );
  };

  return { EntryList };
});

type useEntriesType = typeof useEntries;
type useEntriesReturnType = ReturnType<useEntriesType>;

const mockUseEntries = (values: Partial<useEntriesReturnType> = {}) => {
  const mockedHook = useEntries as jest.MockedFn<typeof useEntries>;

  mockedHook.mockReturnValue({
    convertToCurrency: jest.fn(),
    createEntry: jest.fn(),
    createEntryError: jest.fn(),
    entries: [],
    fetchEntriesError: jest.fn(),
    goToPage: jest.fn(),
    inCurrency: "R$",
    isCreating: false,
    isFetching: false,
    page: 0,
    availableCurrencies: ["USD", "BRL", "EUR"],
    ...values,
  });
};

describe("HomePage", () => {
  describe("when is loading", () => {
    beforeEach(() => {
      mockUseEntries({ isFetching: true });
    });

    it("renders loading", () => {
      render(<HomePage />);

      expect(screen.getByText(/wait for it\.\.\./i)).toBeInTheDocument();
    });

    it("does not render ledgers list", () => {
      render(<HomePage />);

      expect(screen.queryByRole("listitem")).not.toBeInTheDocument();
    });
  });

  describe("when is loaded", () => {
    beforeEach(() => {
      mockUseEntries({
        isFetching: false,
        entries: [
          {
            amount: 123,
            currency: "$",
            date: new Date(2022, 11, 9),
            key: "1",
            ledger: "The Ledger - 1",
            type: EntryType.In,
          },
          {
            amount: 321,
            currency: "R$",
            date: new Date(2022, 11, 9),
            key: "2",
            ledger: "The Ledger - 2",
            type: EntryType.Out,
          },
        ],
      });
    });

    it("does not render loading", () => {
      render(<HomePage />);

      expect(screen.queryByText(/loading.../i)).not.toBeInTheDocument();
    });

    it("renders ledgers list", () => {
      render(<HomePage />);

      const lis = screen.getAllByRole("listitem");

      expect(lis).toHaveLength(2);
      expect(lis[0].innerHTML).toBe("The Ledger - 1");
      expect(lis[1].innerHTML).toBe("The Ledger - 2");
    });
  });
});
