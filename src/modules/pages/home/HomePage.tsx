import { useEntries } from "@/modules/entries";
import { Spinner } from "@/modules/ui/Spinner/Spinner";
import Head from "next/head";
import { EntryList } from "./EntryList";
import { EntryListFilter } from "./EntryListFilter";
import styles from "./HomePage.module.scss";

export const HomePage = () => {
  const {
    entries,
    isFetching,
    page,
    goToPage,
    inCurrency,
    availableCurrencies,
    convertToCurrency,
  } = useEntries();

  const isLoading = entries === undefined || isFetching;

  return (
    <>
      <Head>
        <title>Ledgerdary - Home</title>
      </Head>

      <div className={styles.container}>
        <section className={styles.entriesContainer}>
          {isLoading ? <Spinner /> : <EntryList entries={entries} />}
        </section>

        <EntryListFilter
          page={page}
          inCurrency={inCurrency ?? "Original"}
          availableCurrencies={availableCurrencies}
          onInCurrencySelected={(currency) =>
            currency === "Original"
              ? convertToCurrency(undefined)
              : convertToCurrency(currency)
          }
          onPreviousPageSelected={() => goToPage(page - 1)}
          onNextPageSelected={() => goToPage(page + 1)}
        />
      </div>
    </>
  );
};
