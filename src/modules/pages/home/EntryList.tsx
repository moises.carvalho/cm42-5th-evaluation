import { Entry, EntryType } from "@/modules/entries";
import { format } from "date-fns";
import styles from "./EntryList.module.scss";
import cx from "classnames";

export type EntryListProps = {
  entries: Array<Entry>;
};

export const EntryList = ({ entries }: EntryListProps) => {
  return (
    <ul className={styles.list}>
      {entries.map((entry) => (
        <li className={styles.item} key={entry.key}>
          <div className={styles.itemLeftRow}>
            <div className={styles.ledger}>{entry.ledger}</div>

            <div className={styles.date}>{format(entry.date, "dd/MM/yy")}</div>
          </div>

          <div className={styles.itemRightRow}>
            <div className={styles.currency}>{entry.currency}</div>

            <div
              className={cx(styles.amount, {
                [styles.in]: entry.type === EntryType.In,
                [styles.out]: entry.type === EntryType.Out,
              })}
            >
              {entry.type === EntryType.In ? "+" : "-"}
              {(entry.amount / 100).toFixed(2)}
            </div>
          </div>
        </li>
      ))}
    </ul>
  );
};
