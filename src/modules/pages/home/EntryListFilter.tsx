import styles from "./EntryListFilter.module.scss";
import BackChevronIcon from "@/public/icons/backChevron.svg";
import ForwardChevronIcon from "@/public/icons/forwardChevron.svg";
import { Dropdown } from "@/modules/ui/Dropdown/Dropdown";

export type EntryListFilterProps = {
  page: number;
  onPreviousPageSelected?: () => void;
  onNextPageSelected?: () => void;
  onInCurrencySelected?: (currency: string) => void;
  availableCurrencies: Array<string>;
  inCurrency: string;
};

export const EntryListFilter = ({
  page,
  availableCurrencies,
  inCurrency,
  onNextPageSelected,
  onInCurrencySelected,
  onPreviousPageSelected,
}: EntryListFilterProps) => {
  const inCurrencyOptions = ["Original", ...availableCurrencies];

  return (
    <div className={styles.container}>
      <div className={styles.pageFilter}>
        <button
          disabled={page === 1}
          onClick={onPreviousPageSelected}
          className={styles.pageButton}
        >
          <BackChevronIcon className={styles.pageButtonIcon} />
        </button>

        <div className={styles.page}>{page}</div>

        <button onClick={onNextPageSelected} className={styles.pageButton}>
          <ForwardChevronIcon className={styles.pageButtonIcon} />
        </button>
      </div>

      <Dropdown
        options={inCurrencyOptions}
        label="In Currency:"
        onChange={onInCurrencySelected}
        value={inCurrency}
      />
    </div>
  );
};
