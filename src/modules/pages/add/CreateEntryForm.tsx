import { FormEvent, ReactNode, useState } from "react";

import { Dropdown } from "@/modules/ui/Dropdown/Dropdown";
import { NumberInput } from "@/modules/ui/NumberInput/NumberInput";
import { TextInput } from "@/modules/ui/TextInput/TextInput";
import styles from "./CreateEntryForm.module.scss";
import { useEntries } from "../../entries/view/hooks/useEntries";
import { Entry, EntryType } from "@/modules/entries";
import { values } from "lodash";
import { Button } from "@/modules/ui/Button/Button";

export type CreateEntryFormProps = {};

export const CreateEntryForm = ({}: CreateEntryFormProps) => {
  const { createEntry, createEntryError, isCreating } = useEntries();
  const [ledger, setLedger] = useState<Entry["ledger"]>("");
  const [amount, setAmount] = useState<Entry["amount"]>(0);
  const [type, setType] = useState<Entry["type"]>(EntryType.In);
  const [currency, setCurrency] = useState<Entry["currency"]>("USD");
  const allCurrencies = ["USD", "EUR", "BRL"];

  const resetForm = () => {
    setLedger("");
    setAmount(0);
    setType(EntryType.In);
    setCurrency("USD");
  };

  const onSubmit = async (e: FormEvent) => {
    e.preventDefault();

    if (!ledger) {
      alert("Ledger is required!");
      return;
    } else if (!currency) {
      alert("Currency is required!");
      return;
    } else if (!type) {
      alert("Type is required!");
      return;
    }

    await createEntry({ amount, currency, ledger, type });
    resetForm();
    alert("Entry added!");
  };

  return (
    <form onSubmit={onSubmit} className={styles.container}>
      <FormRow>
        <TextInput
          label="Ledger"
          value={ledger}
          onChange={(e) => setLedger(e.target.value)}
        />
      </FormRow>

      <FormRow>
        <NumberInput
          label="Amount"
          value={amount}
          onChange={(e) => setAmount(Number(e.target.value))}
        />
      </FormRow>

      <FormRow>
        <Dropdown
          options={values(EntryType)}
          label="Type:"
          onChange={setType}
          value={type}
        />
      </FormRow>

      <FormRow>
        <Dropdown
          options={allCurrencies}
          label="Currency:"
          onChange={setCurrency}
          value={currency}
        />
      </FormRow>

      <FormRow>
        <Button disabled={isCreating} type="submit">
          {isCreating ? "Creating entry..." : "Add Entry"}
        </Button>
      </FormRow>
    </form>
  );
};

type FormRowProps = {
  children: ReactNode;
};

const FormRow = ({ children }: FormRowProps) => {
  return <div className={styles.textInputContainer}>{children}</div>;
};
