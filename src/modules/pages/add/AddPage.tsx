import Head from "next/head";
import { CreateEntryForm } from "./CreateEntryForm";
import styles from "./AddPage.module.scss";

export const AddPage = () => {
  return (
    <>
      <Head>
        <title>Ledgerdary - Add</title>
      </Head>

      <section className={styles.section}>
        <CreateEntryForm />
      </section>
    </>
  );
};
