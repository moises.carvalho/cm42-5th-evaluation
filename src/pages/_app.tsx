import type { AppProps } from "next/app";
import "@/modules/styles/global.scss";
import { Layout } from "@/modules/ui/Layout/Layout";
import { EntriesProvider } from "@/modules/entries";

export default function App({ Component, pageProps }: AppProps) {
  return (
    <EntriesProvider>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </EntriesProvider>
  );
}
